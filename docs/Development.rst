.. -*- mode: rst ; ispell-local-dictionary: "american" -*-

Development
===============================


The source of `pdfposter` and its siblings is maintained at
`GitLab <https://gitlab.com>`_. Patches and pull-requests
are hearty welcome.

* You may browse the current repository at the
  `Repository Browser 
  <https://gitlab.com/pdftools/pdfposter>`_

* Or you may check out the current version by running::

    git clone https://gitlab.com/pdftools/pdfposter.git

* Please submit bugs and enhancements to the Issue Tracker at
  <https://gitlab.com/pdftools/pdfjoin/issues>.

**Historical Note:** `pdfposter` was hosted at origo.ethz.ch, but this
site closed at 2012-05-31. Then `pdfposter` was hosted gitorious.org,
which was closed May 2015.
